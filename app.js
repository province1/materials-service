const dotenv = require('dotenv');
dotenv.config();

const express = require('express');
const bodyParser = require('body-parser');
const basicAuth = require('express-basic-auth');
const helmet = require('helmet');
var cors = require('cors');
const morgan = require('morgan');

const {create, transfer, destroy, get_balance} = require('./lib/material');

const app = express();

const credentials = JSON.parse(process.env.CREDENTIALS);

const useBasicAuth = basicAuth({
  users: credentials,
});

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(morgan('combined'));
app.use(cors());

const helmet_config = {crossOriginResourcePolicy: {policy: 'same-site'}};

app.use(helmet(helmet_config));

const port = process.env.PORT || 3000;

// create
app.post('/create', useBasicAuth, async function (req, res, next) {
  try {
    let owner = req.body.owner;
    let amount = req.body.amount;
    let type = req.body.type;

    let response = await create(owner, amount, type);
    res.json(response);
  } catch (err) {
    next(err);
  }
});

// transfer
app.post('/transfer', async function (req, res, next) {
  try {
    const {sender, receiver, amount, type, msg, hash} = req.body;

    let response = await transfer(sender, receiver, amount, type, msg, hash);
    res.json(response);
  } catch (err) {
    next(err);
  }
});

// destroy
app.post('/destroy', useBasicAuth, async function (req, res, next) {
  try {
    let address = req.body.address;
    let type = req.body.type;
    let amount = req.body.amount;

    let response = await destroy(address, type, amount);

    res.json(response);
  } catch (err) {
    next(err);
  }
});

app.get('/balance', useBasicAuth, async function (req, res, next) {
  try {
    let address = req.query.owner;
    let type = req.query.materialType;
    let response = await get_balance(address, type);

    res.json(response);
  } catch (err) {
    next(err);
  }
});

app.use((err, _req, res, _next) => {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

const server = app.listen(port, () => {
  console.log(`${process.env.NAME} listening at http://localhost:${port}`);
});

module.exports = server; // for testing
