const chai = require('chai');
let chaiHttp = require('chai-http');

const should = chai.should();
const expect = chai.expect;
chai.use(chaiHttp);

const supertest = require('supertest')


const tests = require('../lib/helpers.js');




const dotenv = require('dotenv');
dotenv.config();



//

/*
* Test Case Data Construction
*
*/

var msg = "check this message signed by account0"
var signature_good = "0x109dcfc710c8cc878cb0405a002e9528acc24997a241132810bfe5f3c04dce61103b10b0bab30dc745381b770941a320e724aa45fb851e25b94c96948af044c81b"
var address_good = "0x2a26014ff2354c6e5c7dcd944bc3a192E2AC998b"
var address_bad = "0x2a26014ff2354c6e5c7dcd944bc3a192E2AC998c"
var signature_bad = "0x109dcfc710c8cc878cb0405a002e9528acc24997a241132810bfe5f3c04dce61103b10b0bab30dc745381b770941a320e724aa45fb851e25b94c96948af044c81c"


describe('materials service auth tests', function() {

    describe("test for signature verfication", function(){
        
        
        it('Should return true for a valid signature', function(done) {
            tests.verifySignature(msg, signature_good, address_good).should.be.true;
            done()
        })
        
        it('Should throw an error for signature mismatch', function(done) {
            expect(function(){tests.verifySignature(msg, signature_good, address_bad)}).to.throw(Error)
            done()
            
        })
        
        it('Should throw an error for a bad signature', function(done) {
            expect(function(){tests.verifySignature(msg, signature_bad, address_good)}).to.throw(Error)
            done()
        })
        
    })
    
    /*
    * ToDo: Look into what this guy is saying. Had a lot of problems with that really-need package, so didn't follow his advice 100%
    * https://glebbahmutov.com/blog/how-to-correctly-unit-test-express-server/
    */
    
    describe("test for basic auth coverage of applicable routes", function(done){
        
        
        var server
        beforeEach(function () {
            server = require('../app.js');
        });
        /*
        afterEach(function (done) {
            server.close(done);
        });
        */
        
        it('Should reject on wrong credentials', function(done){
            supertest(server)
             .post('/create')
                .auth('Hannah', 'Montanna')
                .send({owner: "0x123", amount: 100, type: "timber"})
                .expect(401, done)
                
            
        })
        
        //load credentials
        const credentials = JSON.parse(process.env.CREDENTIALS)
        var array_key = Object.values(credentials)
        var array_id = Object.keys(credentials)
        
        it('Should reject on wrong credentials', function(done){
            supertest(server)
             .post('/create')
                .auth(array_id[0], array_key[0])
                .send({owner: "0x123", amount: 100, type: "timber"})
                .expect(200)
            server.close(done)
    
        })
        
    })
    
})






