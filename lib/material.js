const {db_service_request} = require('./db_service.js');
const {verifySignature} = require('./helpers');

const material_type_codes = {timber: 0, wood: 1};

async function create(owner, amount, type) {
  let result;
  let owner_address = owner.toLowerCase();
  let action = 'created';

  let db_service_url = process.env.DB_SERVICE_URL + '/insert_single';
  try {
    result = await insert_transaction(owner_address, amount, type, action, db_service_url, null);
  } catch (error) {
    return error;
  }

  return result;
}

async function transfer(sender, receiver, amount, type, msg, hash) {
  sender = sender.toLowerCase();
  let owner_address = receiver.toLowerCase();
  verifySignature(msg, hash, sender);

  let action = 'transfer';
  let db_service_url = process.env.DB_SERVICE_URL + '/insert_single';
  let balance = await get_balance(sender.toLowerCase(), type);
  let result = {};

  if (amount <= balance) {
    try {
      result = await insert_transaction(owner_address, amount, type, action, db_service_url, sender);
    } catch (error) {
      return error;
    }
  } else {
    result = {error: true, error_msg: 'Low balance'};
  }
  return result;
}

async function destroy(address, type, amount) {
  let owner_address = null;
  let sender = address.toLowerCase();

  let action = 'destruction';
  let db_service_url = process.env.DB_SERVICE_URL + '/insert_single';
  let result = {};

  try {
    result = await insert_transaction(owner_address, amount, type, action, db_service_url, sender);
  } catch (error) {
    return error;
  }

  return result;
}

async function get_balance(address, type) {
  let db_service_url = process.env.DB_SERVICE_URL + '/sum_aggregate';
  let result_created_balance = 0;
  let result_transferred_destroyed_balance = 0;
  let balance = 0;

  // check sender's balance
  let data_create = {
    collection: 'materials',
    filter: {
      $and: [{owner: address}, {$or: [{action: 'created'}, {action: 'transferred'}]}, {type}],
    },
  };

  let result_created = (await db_service_request(data_create, db_service_url)).result;

  if (result_created.length > 0) {
    result_created_balance = result_created[0].total;
  }

  let data_send_txs = {
    collection: 'materials',
    filter: {
      $and: [{sender: address}, {$or: [{action: 'transferred'}, {action: 'destruction'}]}, {type}],
    },
  };

  var result_transferred = (await db_service_request(data_send_txs, db_service_url)).result;
  if (result_transferred.length > 0) {
    result_transferred_destroyed_balance = -result_transferred[0].total;
  }

  balance = result_created_balance + result_transferred_destroyed_balance;

  return balance;
}

async function insert_transaction(owner_address, amount, type, action, db_service_url, sender) {
  let timestamp = Date.now();
  let type_code = material_type_codes[type];

  let data = {
    collection: 'materials',
    entry_doc: {
      owner: owner_address,
      amount: amount,
      type: type,
      timestamp,
      action,
      type_code,
      //auction_id
    },
  };
  if (sender != null) {
    data.entry_doc.sender = sender;
  }

  let result = await db_service_request(data, db_service_url);
  return result;
}

module.exports = {create, transfer, destroy, get_balance};
