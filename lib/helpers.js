const Moralis = require('moralis/node');
const ethers = Moralis.web3Library;

function verifySignature(msg, hash, address) {
  //const arrayifiedMsg = ethers.utils.arrayify(msg);
  const signatureAddress = ethers.utils.verifyMessage(msg, hash);
  const validatedAddress = ethers.utils.getAddress(signatureAddress);
  if (address && validatedAddress.toLowerCase() !== address.toLowerCase()) {
    throw new Error('Sender address mismatch');
  } else {
    return true
  }
}

module.exports = {verifySignature};
